namespace AdventOfCode._2021;

public static class IEnumerableExtensions
{
    public static IEnumerable<IEnumerable<T>> RollingWindow<T>(
        this IEnumerable<T> source,
        int windowSize)
    {
        return Enumerable.Range(0, source.Count() - windowSize + 1)
            .Select(i => source.Skip(i).Take(windowSize));
    }
}