using System.Text.RegularExpressions;

namespace AdventOfCode._2021;

public class Day05 : BaseDay
{
    private readonly Line[] _lines;

    public Day05()
    {
        _lines = File.ReadAllLines(InputFilePath)
            .Select(line => new Line(line))
            .ToArray();
    }

    public override ValueTask<string> Solve_1()
    {
        var count = _lines
            .Where(line => line.IsHorizontalOrVertical())
            .SelectMany(line => line.GetPoints())
            .GroupBy(x => x)
            .Where(group => group.Count() > 1)
            .Count();

        return new(count.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var count = _lines
            .SelectMany(line => line.GetPoints())
            .GroupBy(x => x)
            .Where(group => group.Count() > 1)
            .Count();

        return new(count.ToString());
    }

    private class Line
    {
        private readonly (int x, int y) _start;
        private readonly (int x, int y) _end;

        public Line(string input)
        {
            var match = Regex.Match(input, @"(\d+),(\d+) -> (\d+),(\d+)");
            _start = (int.Parse(match.Groups[1].Value), int.Parse(match.Groups[2].Value));
            _end = (int.Parse(match.Groups[3].Value), int.Parse(match.Groups[4].Value));
        }

        public bool IsHorizontalOrVertical()
        {
            return _start.x == _end.x || _start.y == _end.y;
        }

        public (int x, int y)[] GetPoints()
        {
            var xs = NumbersBetween(_start.x, _end.x);
            var ys = NumbersBetween(_start.y, _end.y);

            return xs.Length == ys.Length
                ? xs.Zip(ys).ToArray()
                : xs.SelectMany(x => ys.Select(y => (x, y))).ToArray();
        }

        private int[] NumbersBetween(int start, int end)
        {
            var points = Enumerable.Range(
                Math.Min(start, end),
                Math.Abs(end - start) + 1
            );

            if (end - start < 0)
            {
                points = points.Reverse();
            }

            return points.ToArray();
        }
    }
}
