namespace AdventOfCode._2021;

public class Day03 : BaseDay
{
    private readonly char[][] _reportLogs;

    public Day03()
    {
        _reportLogs = File.ReadAllLines(InputFilePath)
            .Select(log => log.ToCharArray())
            .ToArray();
    }

    public override ValueTask<string> Solve_1()
    {
        var gammaRateBytes = CountZeros(_reportLogs)
            .Select(zerosCount => zerosCount > _reportLogs.Length / 2 ? '0' : '1')
            .ToArray();

        var xor = ToInt(Enumerable.Range(0, gammaRateBytes.Length).Select(_ => '1').ToArray());
        var gammaRate = ToInt(gammaRateBytes);
        var epsilonRate = gammaRate ^ xor;

        return new((gammaRate * epsilonRate).ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var oxygenLogs = _reportLogs.ToArray();
        var co2Logs = _reportLogs.ToArray();

        for (var i = 0; i < oxygenLogs[0].Length; i++)
        {
            if (oxygenLogs.Length != 1)
            {
                var oxygenZeros = CountZerosInPosition(oxygenLogs, i);
                var mostCommonValue = oxygenZeros > oxygenLogs.Length / 2.0 ? '0' : '1';

                oxygenLogs = oxygenLogs.Where(bytes => bytes[i] == mostCommonValue).ToArray();
            }

            if (co2Logs.Length != 1)
            {
                var co2Zeros = CountZerosInPosition(co2Logs, i);
                var leastCommonValue = co2Zeros <= co2Logs.Length / 2.0 ? '0' : '1';

                co2Logs = co2Logs.Where(bytes => bytes[i] == leastCommonValue).ToArray();
            }
        }

        var oxygenRating = ToInt(oxygenLogs.Single());
        var co2Rating = ToInt(co2Logs.Single());

        return new((oxygenRating * co2Rating).ToString());
    }

    private int[] CountZeros(char[][] input)
    {
        return input
            .Aggregate(
                new int[input[0].Length],
                (acc, reportLog) =>
                {
                    for (var i = 0; i < reportLog.Length; i++)
                    {
                        if (reportLog[i] == '0') acc[i]++;
                    }
                    return acc;
                }
            );
    }

    private int CountZerosInPosition(
        char[][] input,
        int position)
    {
        return input.Where(log => log[position] == '0')
            .Count();
    }

    private int ToInt(char[] bytes)
    {
        return Convert.ToInt32(new string(bytes), 2);
    }
}
