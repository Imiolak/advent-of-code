namespace AdventOfCode._2021;

public class Day04 : BaseDay
{
    private readonly (int[] input, BingoBoard[] boards) _game;

    public Day04()
    {
        var lines = File.ReadAllLines(InputFilePath);
        var input = lines[0].Split(',').Select(int.Parse).ToArray();

        var boards = Enumerable.Range(0, (int)Math.Ceiling((lines.Length - 2) / 6.0))
            .Select(i =>
                lines.Skip((i * 6) + 2)
                    .Take(5)
                    .Select(line =>
                        line.Split(' ')
                            .Where(c => !string.IsNullOrWhiteSpace(c))
                            .Select(int.Parse)
                            .ToArray())
                    .ToArray())
            .Select(board => new BingoBoard(board))
            .ToArray();

        _game = (input, boards);
    }

    public override ValueTask<string> Solve_1()
    {
        var (firstWinningBoard, lastCalledNumber) = PlayBingo(_game.boards, _game.input).First();
        var sumOfUnmarked = firstWinningBoard.GetUnmarkedNumbers().Sum();

        return new((sumOfUnmarked * lastCalledNumber).ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        foreach (var board in _game.boards)
        {
            board.Reset();
        }

        var (lastWinningBoard, lastCalledNumber) = PlayBingo(_game.boards, _game.input).Last();
        var sumOfUnmarked = lastWinningBoard.GetUnmarkedNumbers().Sum();

        return new((sumOfUnmarked * lastCalledNumber).ToString());
    }

    private (BingoBoard board, int lastCalledNumber)[] PlayBingo(
        BingoBoard[] boards,
        int[] input)
    {
        var winners = new List<(BingoBoard, int)>();
        var participants = boards.ToArray();

        foreach (var number in input)
        {
            foreach (var board in participants)
            {
                board.Mark(number);
                if (board.IsBingo())
                {
                    winners.Add((board, number));
                }
            }

            participants = participants.Where(b => !b.IsBingo()).ToArray();
        }

        return winners.ToArray();
    }

    private class BingoBoard
    {
        private const int BoardSize = 5;

        private readonly int[][] _numbers;
        private bool[][] _marked;

        public BingoBoard(int[][] numbers)
        {
            if (numbers.Length != BoardSize)
                throw new ArgumentException($"Board must have {BoardSize} rows");
            if (numbers.Any(row => row.Length != 5))
                throw new ArgumentException($"All rows must have {BoardSize} values");

            _numbers = numbers;
            Reset();
        }

        public void Mark(int number)
        {
            for (var i = 0; i < BoardSize; i++)
            {
                for (var j = 0; j < BoardSize; j++)
                {
                    if (_numbers[i][j] == number)
                    {
                        _marked[i][j] = true;
                    }
                }
            }
        }

        public bool IsBingo()
        {
            return _marked
                .Concat(Enumerable.Range(0, BoardSize)
                    .Select(col => Enumerable.Range(0, BoardSize)
                        .Select(row => _marked[row][col]).ToArray()))
                .ToArray()
                .Any(x => x.All(marked => marked == true));
        }

        public int[] GetUnmarkedNumbers()
        {
            var result = new List<int>();

            for (var i = 0; i < BoardSize; i++)
            {
                for (var j = 0; j < BoardSize; j++)
                {
                    if (_marked[i][j] == false)
                        result.Add(_numbers[i][j]);
                }
            }

            return result.ToArray();
        }

        public void Reset()
        {
            _marked = Enumerable.Range(0, BoardSize)
                            .Select(_ => new bool[BoardSize])
                            .ToArray();
        }
    }
}
