namespace AdventOfCode._2021;

public class Day02 : BaseDay
{
    private readonly (string direction, int offset)[] _commands;

    public Day02()
    {
        _commands = File.ReadAllLines(InputFilePath)
            .Select(line =>
            {
                var split = line.Split(' ');
                return (split[0], int.Parse(split[1]));
            })
            .ToArray();
    }

    public override ValueTask<string> Solve_1()
    {
        var finalCoords = _commands.Aggregate(
            (position: 0, depth: 0),
            (coords, command) =>
                command.direction switch
                {
                    "forward" => (coords.position + command.offset, coords.depth),
                    "down" => (coords.position, coords.depth + command.offset),
                    "up" => (coords.position, coords.depth - command.offset),
                    _ => throw new ArgumentException()
                });

        return new((finalCoords.position * finalCoords.depth).ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var finalCoords = _commands.Aggregate(
            (position: 0, depth: 0, aim: 0),
            (coords, command) =>
                command.direction switch
                {
                    "forward" => (coords.position + command.offset, coords.depth + (coords.aim * command.offset), coords.aim),
                    "down" => (coords.position, coords.depth, coords.aim + command.offset),
                    "up" => (coords.position, coords.depth, coords.aim - command.offset),
                    _ => throw new ArgumentException()
                });

        return new((finalCoords.position * finalCoords.depth).ToString());
    }
}
