﻿namespace AdventOfCode._2021;

public class Day01 : BaseDay
{
    private readonly int[] _depths;

    public Day01()
    {
        _depths = File.ReadAllLines(InputFilePath)
            .Select(int.Parse)
            .ToArray();
    }

    public override ValueTask<string> Solve_1()
    {
        var result = _depths.RollingWindow(windowSize: 2)
            .Count(pair => pair.ElementAt(1) > pair.ElementAt(0));

        return new(result.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var result = _depths.RollingWindow(windowSize: 3)
            .Select(range => range.Sum())
            .RollingWindow(windowSize: 2)
            .Count(pair => pair.ElementAt(1) > pair.ElementAt(0));

        return new(result.ToString());
    }
}
