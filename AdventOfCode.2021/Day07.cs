using System.Text.RegularExpressions;

namespace AdventOfCode._2021;

public class Day07 : BaseDay
{
    private readonly int[] _positions;

    public Day07()
    {
        _positions = File.ReadAllLines(InputFilePath)[0]
            .Split(',')
            .Select(int.Parse)
            .ToArray();
    }

    public override ValueTask<string> Solve_1()
    {
        var minPos = _positions.Min();
        var maxPos = _positions.Max();

        var minFuel = Enumerable.Range(minPos, maxPos - minPos + 1)
            .Select(dest => _positions.Aggregate(0, (sum, pos) => sum += Math.Abs(dest - pos)))
            .Min();

        return new(minFuel.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var minPos = _positions.Min();
        var maxPos = _positions.Max();

        var minFuel = Enumerable.Range(minPos, maxPos - minPos + 1)
            .Select(dest => _positions.Aggregate(0M, (sum, pos) => sum += FuelCost(pos, dest)))
            .Min();

        return new(minFuel.ToString());
    }

    private int FuelCost(int position, int destination)
    {
        return Enumerable.Range(1, Math.Abs(destination - position)).Sum();
    }
}
