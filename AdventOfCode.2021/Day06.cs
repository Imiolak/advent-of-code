using System.Text.RegularExpressions;

namespace AdventOfCode._2021;

public class Day06 : BaseDay
{
    private readonly decimal[] _fishCounts;

    public Day06()
    {
        var timers = File.ReadAllLines(InputFilePath)[0]
            .Split(',')
            .Select(int.Parse);

        _fishCounts = new decimal[9];

        foreach (var timer in timers)
        {
            _fishCounts[timer]++;
        }
    }

    public override ValueTask<string> Solve_1()
    {
        var fishCounts = _fishCounts.ToArray();

        for (var i = 0; i < 80; i++)
        {
            fishCounts = SimulateDay(fishCounts);
        }

        var populationCount = fishCounts.Sum();
        return new(populationCount.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var fishCounts = _fishCounts.ToArray();

        for (var i = 0; i < 256; i++)
        {
            fishCounts = SimulateDay(fishCounts);
        }

        var populationCount = fishCounts.Sum();
        return new(populationCount.ToString());
    }

    private decimal[] SimulateDay(decimal[] population)
    {
        var newPopulation = new decimal[population.Length];
        var readyToReproduce = population[0];

        newPopulation[newPopulation.Length - 3] += readyToReproduce;
        newPopulation[newPopulation.Length - 1] += readyToReproduce;

        for (int i = 0; i < population.Length - 1; i++)
        {
            newPopulation[i] += population[i + 1];
        }

        return newPopulation;
    }
}
